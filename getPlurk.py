import json
import requests
from datetime import datetime
from time import sleep

## common strrings
plurk_time_rec_format = '%a, %d %b %Y %H:%M:%S GMT'
plurk_time_sen_format = '%Y-%m-%dT%H:%M:%S.000Z'

def savePlurk(filename,plurks,includeReplurk=True):
    logfile = open( filename, "a")
    for plurk in plurks:
        if (includeReplurk):
            logfile.writelines(json.dumps(plurk)+"\n")
        else:
            if (plurk['user_id']==plurk['owner_id']):
                logfile.writelines(json.dumps(plurk)+"\n")

def getPlurks(uid,offset=None):
    url = 'https://www.plurk.com/TimeLine/getPlurks'
    if (offset==None):
        payload = {'user_id': uid}
    else:
        payload = {'user_id': uid, 'offset': offset}
    r = requests.post(url, data=payload)

    nr = r.text.replace("new Date(","").replace(")","")
    return json.loads(nr)

def getAllPlurks(uid,filename,includeReplurk=True):
    finished = False
    offset = None

    open(filename,'w')
    
    while (not finished):
        plurks = getPlurks(uid,offset)['plurks']
        savePlurk(filename,plurks,includeReplurk)
    
        if len(plurks) == 20:
            timeStr = plurks[19]['posted']
            timeObj = datetime.strptime(timeStr, plurk_time_rec_format)
            timeStrOutput = datetime.strftime(timeObj, plurk_time_sen_format)
            offset = timeStrOutput
            print(offset)
            print("still something")
    
        else:
            print('maybe ended, only '+str(len(plurks))+' plurks')
            finished = True
            break
    
        sleep(5)

def convertCSV(inputFilename,outputFilename):
    import csv
    
    with open(outputFilename,'w') as csvFile:
        writer = csv.writer(csvFile, delimiter=',', quotechar='\'')
        jsonFile = open(inputFilename,'r')
        writer.writerow(["日期","是否為轉噗","內容"])
        for i in jsonFile:
            data = json.loads(i)
            writer.writerow([data['posted'],(not data['user_id']==data['owner_id']),data['content_raw']])

getAllPlurks('4532615','plurks.txt',False)
convertCSV('plurks.txt','plurks.csv')

